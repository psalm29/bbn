
jQuery(document).ready(function() {

            function p_spinIn() {
    $('#p_spinner').css('display', 'inline');
}

function p_spinOut() {
    $('#p_spinner').css('display', 'none');
}

function r_spinIn() {
    $('#r_spinner').css('display', 'inline');
}

function r_spinOut() {
    $('#r_spinner').css('display', 'none');
}

function s_spinIn() {
    $('#s_spinner').css('display', 'inline');
}

function s_spinOut() {
    $('#s_spinner').css('display', 'none');
}

function spinIn() {
    $('#spinner').css('display', 'inline');
}

function spinOut() {
    $('#spinner').css('display', 'none');
}

function l_spinIn() {
    $('#l_spinner').css('display', 'inline');
}

function l_spinOut() {
    $('#l_spinner').css('display', 'none');
}

            var url = 'http://178.62.41.199/bulksms/';
            //  var url = 'http://sms.bbnplace.com/new/api/';
            var app_id = '10000';

            //User Login

            $('#loginBtn').click(function() {
                var username = $('#username').val();
                var password = $('#password').val();

                if (username == '' && password == '') {
                    alert('Enter your password and email address or mobile number');
                }else{
                    l_spinIn();


                        $.post(url+'/users/login.json', {'app_id': app_id, 'username': username, 'password': password}).done( function (data) {
        
        if (data.response.session_token == null) {
            alert(data.response.error);
        }else{
            var session_token = data.response.session_token;
            localStorage.setItem('session_token', session_token);
            window.location.replace('user.html');
        }

l_spinOut();
                });
                }

            });

//request signup token
$('#signup_request_btn').click(function() {
                s_spinIn();
                var phone = $('#s_phone').val();
                var token = $('#s_token').val();

                if ($('#s_token').css('display') == 'none') {

                    if (phone == '') {
                        alert('Enter your mobile number');
                    }else{
                        $.getJSON( url+'/users/initiate_signup/'+phone+'.json', function (data) {
        alert(data.response);

        if (data.response == "Token Sent") {
            $('#s_token').css('display', 'block');
        }

s_spinOut();
                });
                    }


    
                }else{
                    
if (phone == '' && token == '') {
                        alert('Enter your mobile number and sign up token');
                    }else{
s_spinIn();

                        $.post( url+'/users/verify_mobile.json', {'mobile': phone, 'token': token}).done( function (data) {
        alert(data.response);

        if (data.response == "Token Sent") {
            $('#s_token').css('display', 'block');
        }

s_spinOut();
                });
                    }

    }
    

            });

            //reset password
            $('#password_request_form').submit(function() {
                var phone = $('#p_phone').val();

                if (phone == '') {
                    alert('Enter Mobile Number');
                }else{

p_spinIn();

    $.getJSON( url+'/users/request_token/'+phone+'.json', function (data) {
        //console.log(data.response.message);

        if (data.response.message == "token generated") {
            $('#r_mobile').val(phone);
$('#p_phone').val('');
$('#passwordModal').removeClass('active');
$('#password22').addClass('active');
alert('Password reset token generated')
        }else{
            alert(data.response.message);
        }

p_spinOut();
        
    });
                    
                }
                

    return false;

            });


//Request password reset token

$('#password_request_form2').submit(function(event) {
  
  $token = $('#r_token').val();
  $pwd = $('#r_pwd').val();
  $mobile = $('#r_mobile').val();
  $pwd_repeat = $('#r_pwd_repeat').val();

  if ($token == '' || $pwd == '' || $pwd_repeat == '' || $mobile == '') {
    alert('All fields are required');
  }else{

      if ($pwd != $pwd_repeat) {
        alert('Your password is not the same with your repeated password');
      }else{

r_spinIn();

          $.ajax( url+'users/reset_password.json', {'token': $token, 'mobile': $mobile, 'password': $pwd} ).done( function (data) {

    if (data.response == 'Saved') {
      $('#r_token, #r_pwd, #r_pwd_repeat').val('');
      $('#password22').removeClass('active');
      $('#passwordModal').removeClass('active');
      alert('Password successfully changed');
    }else{
      alert(data.response);
    }
    
    r_spinOut();
});

      }

  }


});




});
        
